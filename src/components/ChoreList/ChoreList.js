import React from 'react';
import PropTypes from 'prop-types';
import Chore from '../Chore/Chore';
import './ChoreList.css';

class ChoreList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      chores: []
    };
  }

  componentWillReceiveProps(props) {
    this.setState({chores: props.chores});
  }

  render() {
    return (
      <div className="chore-list">
        <div className="list-title"><h1>Chorelist</h1></div>
        {this.state.chores.map( chore => {
            return <Chore  key={chore.id} chore={chore} update={this.props.update}/>;
        })}
      </div>
    );
  }
}

export default ChoreList;
