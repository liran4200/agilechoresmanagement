import React from 'react';
import PropTypes from 'prop-types';
import './Chore.css';

class Chore extends React.Component {
  static propTypes = {

  };

  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: "",
      description: "",
      isAssigned: false,
      roommateName: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.hoverMessage = this.hoverMessage.bind(this);
  }

  handleClick(event) {
    var isAssigned;
    var roommateName;
    if (this.state.isAssigned == false && this.state.roommateName == "") {
      isAssigned = true;
      roommateName = "me";
    } else if (this.state.isAssigned == true && this.state.roommateName == "me") {
      isAssigned = false;
      roommateName = "";
    }
    this.props.update(this.props.chore.id, isAssigned, roommateName);
  }

  componentDidMount() {
    this.setState({
      id: this.props.id,
      name: this.props.chore.name,
      description: this.props.chore.description,
      isAssigned: this.props.chore.isAssigned,
      roommateName: this.props.chore.roommateName
    });
  }

  componentWillReceiveProps(props){

  }

  hoverMessage() {
    if (this.state.isAssigned == false && this.state.roommateName == "") {
      return "assign to me";
    } else if (this.state.isAssigned == true) {
      if (this.state.roommateName == "me") {
        return "unassign";
      } else {
        return "this chore is already assigned";
      }
    }
  }

  render() {
    return (
        <div className="chore-item" onClick={this.handleClick}>
            <div className="title">
              <div className="chore-name">{this.state.name}</div>
              <input type="checkbox" name="" value="" ></input>
            </div>
            <div className="chore-description">{this.state.description}</div>
            <div className="roommate-name">assigned to: {this.state.roommateName}</div>
            <div className="assign-to-label">{this.hoverMessage()}</div>
        </div>
    );
  }
}

export default Chore;
