import React, { Component } from 'react';
import './App.css';
import ChoreList from '../ChoreList/ChoreList'
import AssignedChoresList from '../AssignedChoresList/AssignedChoresList'

class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
        chores: [],
        assignedChores: []
    };
    this.allChores = [];
    this.choreAssignedHandler = this.choreAssignedHandler.bind(this);
  }

  choreAssignedHandler(id, isAssigned, roommateName) {
    var index = this.getChoreIndexById(id);
    this.allChores[index].isAssigned = isAssigned;
    this.allChores[index].roommateName = roommateName;

    this.setState({
      assignedChores: this.getAssignedChores(),
      chores: this.getNotAssigneChores()
    });
  }

  getChoreIndexById(choreId) {
    var index = 0;
    for (var index in this.allChores) {
      var chore = this.allChores[index];
      if (chore.id == choreId) {
        return index;
      }
    }
  }

  getAssignedChores() {
    return this.allChores.filter(chore => chore.isAssigned == true && chore.roommateName == "me");
  }

  getNotAssigneChores() {
    return this.allChores.filter(chore => chore.roommateName != "me");
  }

  componentDidMount() {
    this.chores = this.getChores();
  }

  getChores() {
     this.allChores = [
                  {
                      "id": 1000,
                      "name": "Garbge",
                      "description": "Throwing away garbge every day",
                      "createDate": new Date(),
                      "expirationDate": null,
                      "isAssigned": false,
                      "roommateName": ""
                  },
                  {
                      "id": 1001,
                      "name": "Kitchen",
                      "description": "Cleaning all the kitchen",
                      "createDate": new Date(),
                      "expirationDate": null,
                      "isAssigned" : false,
                      "roommateName": ""
                  },
                  {
                      "id": 1002,
                      "name": "Clean TV in living room",
                      "description": "Clean dast on TV with special metrial",
                      "createDate": new Date(),
                      "expirationDate": null,
                      "isAssigned" : true,
                      "roommateName": "Liran Yehudar"
                  },
                  {
                      "id": 1003,
                      "name": "Clean Nir room",
                      "description": "Cleaning desk",
                      "createDate": new Date(),
                      "expirationDate": null,
                      "isAssigned" : true,
                      "roommateName": "Nir Finz"
                  }
    ];

    this.setState({
      assignedChores: this.getAssignedChores(),
      chores: this.getNotAssigneChores()
    });

  }

  render() {
    return (
      <div className="App">
        <ChoreList chores={this.state.chores} update={this.choreAssignedHandler}/>
        <AssignedChoresList assignedChores={this.state.assignedChores} update={this.choreAssignedHandler}/>
      </div>
    );
  }

}

export default App;
