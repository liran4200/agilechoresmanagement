import React from 'react';
import PropTypes from 'prop-types';
import Chore from '../Chore/Chore';
import './AssignedChoresList.css';

class AssignedChoreList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        assignedChores: []
      };
  }

  componentWillReceiveProps(props) {
    this.setState({assignedChores: props.assignedChores});
  }

  render() {
    return (
      <div className="assigned-chores">
        <div className="assigned-list-title"><h1>My Assigned Chores</h1></div>
          {this.state.assignedChores.map( chore => {
            return <Chore  key={chore.id} chore={chore} update={this.props.update}/>;
          })}
      </div>
    );
  }

}

export default AssignedChoreList;
